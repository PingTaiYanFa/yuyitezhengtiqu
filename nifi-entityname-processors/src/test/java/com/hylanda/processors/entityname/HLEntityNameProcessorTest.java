/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hylanda.processors.entityname;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Before;
import org.junit.Test;

public class HLEntityNameProcessorTest {

	private TestRunner testRunner;

	@Before
	public void init() {
		testRunner = TestRunners.newTestRunner(HLEntityNameProcessor.class);
	}

	@Test
	public void testProcessor() {
		// set property
		/*testRunner.setProperty("newsegment.dictPath", "config/");
		testRunner.setProperty("processor.hlentityname.locPath", "config/");
		testRunner.setProperty("deal_fields_name", "fmt_format_content");
		testRunner.setProperty("deal_fields_loc", "fmt_format_content");
		testRunner.setProperty("deal_fields_org", "fmt_format_content");
    	testRunner.setProperty("loc_type", "[\"景点坐标村庄道路类地名\"]");
		//testRunner.setProperty("black_words", "邓小平");
		testRunner.setProperty("white_words", "飞轮海,毛泽东");
		testRunner.setProperty("processor.hlentityname.configPath", "F:\\haojing\\Works\\NIFI\\RISNifi\\entityname\\nifi-entityname-processors\\config\\");
		//testRunner.setProperty("keyword_cnt","10");
		testRunner.setProperty("keyword_dict","天津海量信息技术");
		testRunner.setProperty("deal_fields_keyword", "fmt_format_content");
		// add content to queue
		String content = "{\"fmt_title\":\"老毛，小邓，毛泽东、邓小平，飞轮海。\",\"fmt_format_content\":\"李明是个好孩子，他考上了北京大学。\"}";
		InputStream isContent = new ByteArrayInputStream(content.getBytes());

		testRunner.enqueue(isContent);
		testRunner.run();*/

	}

}
