package com.hylanda.tools;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;

public class CommonTools {
	
	private static final String FILENAME_KEYWORD = "keyword.txt";
	
	public static String buildUsrDict(String dictPath, String savePath, String id, String usrWords, String wordType){
		
		//否则，需要将用户填的关键词知识和系统自带的关键词词典一起合并成新的文件进行加载
		String dictfile = savePath.concat(id).concat(FILENAME_KEYWORD);
		File file = new File(dictfile);
		if(file.exists()){
			file.delete();
		}
		
		usrWords = usrWords.replaceAll("，", "");
		
		try {
			FileUtils.copyFile(new File(dictPath.concat(FILENAME_KEYWORD)), new File(dictfile));
			
			String [] words = usrWords.split(",");
			for(String word : words){
				if(!StringUtils.isEmpty(word)){
					if(StringUtils.isBlank(wordType)){
						FileUtils.write(file, "\n".concat(word), "utf8", true);
					}else{
						FileUtils.write(file, "\n".concat(word).concat("\t").concat(wordType), "utf8", true);
					}
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			dictfile = null;
		}
		
		return dictfile;
	}

}
