package com.hylanda.entity.name;

import java.util.Map;

public class SegmentProcess {
	
	/**将识别出的人名结果添加到结果集合中
	 * @param field
	 * @param word
	 * @param offset
	 * @param resultList
	 */
	
	public static void addResult(String word, Map<String, Integer> conutMap) {

		if(conutMap.containsKey(word)){
			conutMap.put(word, conutMap.get(word) + 1);
		}else{
			conutMap.put(word, 1);
		}
	}

}
