/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hylanda.entity.loc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hylanda.loc.MultiLevelNormalize;
import com.hylanda.loc.MultiLevelNormalize.NormalResult;
import com.hylanda.tool.Poi;


public class HLLocationProcess{

	private Set<String> dealFieldSet = new HashSet<>();

	private MultiLevelNormalize normalize = MultiLevelNormalize.getInstance();
	
	private final String FIELD_SUFFIX_LOC = "_locs";


	private boolean region = false;
	private boolean abroad = false;
	private boolean hotpoi = false;


	public boolean init(String dictpath, String locType, Set<String> fieldSet) {

		boolean ret = false;
		
		if(StringUtils.isEmpty(locType) || fieldSet.isEmpty()){
			return true;
		}
		
		dealFieldSet = fieldSet;
		
		JSONArray array = JSONArray.parseArray(locType);
		region = array.contains("全国行政区划地名") ? true : false;
		abroad = array.contains("外国地名") ? true : false;
		hotpoi = array.contains("景点坐标村庄道路类地名") ? true : false;

		ret = locInit(dictpath, true, abroad, true);
		
		return ret;
	}


	public boolean doLocRecognition(Map<String, String> dataMap) {

		boolean ret = false;
		// 看下预处理的结果，来判断下是否需要进行分词的处理
		for (String field : dealFieldSet) {
			String content = dataMap.get(field);
			if (StringUtils.isEmpty(content)) {
				continue;
			}

			JSONArray array = getLocName(field, content);
			if(array.isEmpty()){
				continue;
			}
			
			ret = true;
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("data_type", "ne_loc");
			jsonObject.put("datas", array);
			
			dataMap.put(field.concat(FIELD_SUFFIX_LOC), jsonObject.toJSONString());
		}
		
		return ret;

	}
	
	/**
	 * @param dictpath
	 * @return
	 */
	private boolean locInit(String dictpath, boolean regionLoc, boolean abroad, boolean hotpoi) {

		if (StringUtils.isEmpty(dictpath)) {
			return false;
		}

		boolean ret = true;
		String region = regionLoc ? dictpath + "regindict.txt" : null;
		String hotPoint = hotpoi ? dictpath + "hotpointdict.txt" : null;
		String foreign = abroad ? dictpath + "foreigndict.txt" : null;
		String place = hotpoi ? dictpath + "placedict.txt" : null;
		String black = dictpath + "blackdict.txt";

		ret = normalize.init(region, hotPoint, foreign, place, black);

		return ret;
	}

	private JSONArray getLocName(String field, String content) {

		JSONArray array = new JSONArray();
		List<NormalResult> NormalResultlist = new ArrayList<NormalResult>();
		NormalResultlist = normalize.normalize(content, 2, false);
		List<Poi> list_dict = new ArrayList<Poi>();
		list_dict = normalize.standardizedResult(NormalResultlist);
		
		Map<String, Integer> countMap = new HashMap<>();
		Map<String, JSONObject> jsonMap = new HashMap<>();

		for (Poi poi : list_dict) {

			String locname = poi.getName();

			if (locname.length() == 1) {
				continue;
			}

			if (poi.getMapString().equals("中华人民共和国")) {
				poi.setMapString("中国");
			}

			// 判断下要的类别
			int mask = poi.getMapLevel();

			String fmt = poi.getMapString();

			if (poi.getAreaID().equals("0")) {
				// 说明是外国地名
				mask = 16;
				fmt = StringUtils.isEmpty(fmt) ? poi.getName() : fmt;
			} else {
				// 非行政区划地名
				mask = poi.getMapLevel() == -1 ? 6 : poi.getMapLevel();
			}

			if(matchCondition(mask)){
				JSONObject object = new JSONObject();
				object.put("str", poi.getName());
				object.put("maplevel", fmt);
				object.put("mask", mask);
				
				String string = fmt.concat("-").concat(poi.getName());
				if(countMap.containsKey(string)){
					countMap.put(string, countMap.get(string) + 1);
				}else{
					countMap.put(string, 1);
				}
				
				if(!jsonMap.containsKey(string)){
					jsonMap.put(string, object);
				}
			}
			


		}
		
		for(Entry<String, JSONObject> entry : jsonMap.entrySet()){
			JSONObject jsonObject = entry.getValue();
			jsonObject.put("count", countMap.get(entry.getKey()));
			array.add(jsonObject);
		}
		
		return array;

	}

	private boolean matchCondition(int mask) {
		boolean addresult = false;

		if (region && mask <= 5) {
			// 如果要行政区划地名
			addresult = true;
		} else if (abroad && mask == 16) {
			addresult = true;
		} else if (hotpoi && mask == 6) {
			addresult = true;
		}

		return addresult;
	}

	public List<String> genLocationField(String sourceFields){
		ArrayList<String> fieldsList = new ArrayList<>();
		String[] fields = sourceFields.split(",");
		for(String field: fields) {
			fieldsList.add(field.trim().concat(FIELD_SUFFIX_LOC));
		}
		return fieldsList;
	}
}
